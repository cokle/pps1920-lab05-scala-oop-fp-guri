package u05lab.code

import u05lab.code.Kind.Kind

import scala.collection.mutable

object Kind extends Enumeration {
  type Kind = Value

  val RETIRED = Value("RETIRED")
  val FAILED = Value("FAILED")
  val SUCCEEDED = Value("SUCCEEDED")

  override def toString(): String = super.toString()+"d"
}

trait ExamResult {
  def getKind: Kind
  def getEvaluation: Option[Integer]
  def cumLaude: Boolean
}

object ExamResult{
  def apply(kind: Kind, vote: Option[Integer]): ExamResult = new ExamResultImpl(kind, vote)
  def apply (kind: Kind): ExamResult = new ExamResultImpl(kind)
}

class ExamResultImpl(kind: Kind, vote: Option[Integer]) extends ExamResult {

  def this(kind: Kind) {
    this(kind, None)
  }

  override def getKind: Kind = kind

  override def getEvaluation: Option[Integer] = vote

  override def cumLaude: Boolean = getKind.equals(Kind.SUCCEEDED) && getEvaluation.get >= 30

  override def toString: String =
    if (getKind.equals(Kind.SUCCEEDED)) {
      if (cumLaude)  getKind.toString+ "(30L)" else getKind.toString + "(" + getEvaluation.get + ")"
    } else getKind.toString

}


trait ExamResultFactory {
  def failed: ExamResult
  def retired: ExamResult
  def succeededCumLaude: ExamResult
  def succeeded(evaluation: Int): ExamResult
}

object ExamResultFactory {
  def apply(): ExamResultFactory = ExamResultFactoryImpl()
}

case class ExamResultFactoryImpl() extends ExamResultFactory {

  private val withLaude:Option[Integer] = Some(30)

  override def failed: ExamResult = ExamResult(Kind.FAILED)

  override def retired: ExamResult = ExamResult(Kind.RETIRED)

  override def succeededCumLaude: ExamResult = ExamResult(Kind.SUCCEEDED, withLaude)

  override def succeeded(evaluation: Int): ExamResult = if(evaluation<18 || evaluation > 30) throw new IllegalArgumentException
                                                        else ExamResult(Kind.SUCCEEDED, Some(evaluation))
}


trait ExamsManager {
  def createNewCall(call: String)
  def addStudentResult(call: String, student: String, result: ExamResult)
  def getAllStudentsFromCall(call: String): Set[String]
  def getEvaluationsMapFromCall(call: String): Map[String, Integer]
  def getResultsMapFromStudent(student: String): Map[String, String]
  def getBestResultFromStudent(student: String): Option[Integer]
}

object ExamsManager {
  def apply(): ExamsManager =  ExamsManagerImpl()
}

case class ExamsManagerImpl() extends ExamsManager {

  private var studentsResult: mutable.Map[String, mutable.Map[String, ExamResult]] = mutable.Map[String, mutable.Map[String, ExamResult]]()

  override def createNewCall(call: String): Unit = if (studentsResult.contains(call)) throw new IllegalArgumentException
                                                  else studentsResult += (call -> mutable.Map())

  override def addStudentResult(call: String, student: String, result: ExamResult): Unit = if (studentsResult(call) contains student) throw new IllegalArgumentException
                                                                                            else studentsResult(call) ++= mutable.Map(student -> result)

  override def getAllStudentsFromCall(call: String): Set[String] = studentsResult(call).keys.toSet

  override def getEvaluationsMapFromCall(call: String): Map[String, Integer] = studentsResult(call).filter(e => e._2.getEvaluation.isDefined).map({
    case (k, v) => (k, v.getEvaluation.get)}).toMap

  override def getResultsMapFromStudent(student: String): Map[String, String] = studentsResult.filter(e => e._2.contains(student)).map({
    case (k, v) => (k, v(student).toString)}).toMap

  override def getBestResultFromStudent(student: String): Option[Integer] = studentsResult.filter(e => e._2.contains(student)) // check if contains student
                                                .map({ case (k, v) => v(student).getEvaluation}) // transform the map in a Iterable of values of Evaluations Iterable[Option[Integer]]
                                                .filter(o => o.isDefined) // filter by evaluations is defined
                                                .toList // convert to list
                                                .sortWith(_.get > _.get) // sort in DES order
                                                match {  // pattern match to control if list is empty otherwise return first elem
                                                    case Nil => Option.empty
                                                    case h::_ => h
                                                }


}