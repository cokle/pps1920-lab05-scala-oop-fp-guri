package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.function.Executable
import u05lab.code._

class ExamsManagerTest {

  val  erf = ExamResultFactoryImpl()
  val  em = new ExamsManagerImpl()


  // verifica base di ExamResultFactory
  @Test
  def testExamResultsBasicBehaviour(): Unit = {
    // esame fallito, non c'è voto
    assertEquals(erf.failed.getKind, Kind.FAILED);
    assertTrue(erf.failed.getEvaluation.isEmpty);
    assertFalse(erf.failed.cumLaude);
    assertEquals(erf.failed.toString(), "FAILED");

    // lo studente si è ritirato, non c'è voto
    assertEquals(erf.retired.getKind, Kind.RETIRED);
    assertTrue(erf.retired.getEvaluation.isEmpty);
    assertFalse(erf.retired.cumLaude);
    assertEquals(erf.retired.toString(), "RETIRED");

    // 30L
    assertEquals(erf.succeededCumLaude.getKind, Kind.SUCCEEDED);
    assertEquals(erf.succeededCumLaude.getEvaluation, Some(30));
    assertTrue(erf.succeededCumLaude.cumLaude);
    assertEquals(erf.succeededCumLaude.toString(), "SUCCEEDED(30L)");

    // esame superato, ma non con lode
    assertEquals(erf.succeeded(28).getKind, Kind.SUCCEEDED);
    assertEquals(erf.succeeded(28).getEvaluation, Some(28));
    assertFalse(erf.succeeded(28).cumLaude);
    assertEquals(erf.succeeded(28).toString(), "SUCCEEDED(28)");
  }

  // verifica eccezione in ExamResultFactory
  @Test
  def optionalTestEvaluationCantBeGreaterThan30(): Unit = {
    assertThrows(classOf[IllegalArgumentException], new Executable {
      override def execute(): Unit = {
        erf.succeeded(32);
      }
    })
  }

  // verifica eccezione in ExamResultFactory
  @Test
  def  optionalTestEvaluationCantBeSmallerThan18(): Unit = {
    assertThrows(classOf[IllegalArgumentException], new Executable {
      override def execute(): Unit = {
        erf.succeeded(17);
      }
    })
  }

    // metodo di creazione di una situazione di risultati in 3 appelli
    def  prepareExams():Unit = {
      em.createNewCall("gennaio");
      em.createNewCall("febbraio");
      em.createNewCall("marzo");

      em.addStudentResult("gennaio", "rossi", erf.failed); // rossi -> fallito
      em.addStudentResult("gennaio", "bianchi", erf.retired); // bianchi -> ritirato
      em.addStudentResult("gennaio", "verdi", erf.succeeded(28)); // verdi -> 28
      em.addStudentResult("gennaio", "neri", erf.succeededCumLaude); // neri -> 30L


      em.addStudentResult("febbraio", "rossi", erf.failed); // etc..
      em.addStudentResult("febbraio", "bianchi", erf.succeeded(20));
      em.addStudentResult("febbraio", "verdi", erf.succeeded(30));

      em.addStudentResult("marzo", "rossi", erf.succeeded(25));
      em.addStudentResult("marzo", "bianchi", erf.succeeded(25));
      em.addStudentResult("marzo", "viola", erf.failed);

    }

    // verifica base della parte obbligatoria di ExamManager
    @Test
    def testExamsManagement():Unit =  {
      this.prepareExams();
      // partecipanti agli appelli di gennaio e marzo
      assertEquals(em.getAllStudentsFromCall("gennaio"), Set("rossi","bianchi","verdi","neri"));
      assertEquals(em.getAllStudentsFromCall("marzo"), Set("rossi","bianchi","viola"));

      // promossi di gennaio con voto
      println((em.getEvaluationsMapFromCall("gennaio")))
      assertEquals(em.getEvaluationsMapFromCall("gennaio").size, 2);
      assertEquals(em.getEvaluationsMapFromCall("gennaio")("verdi"), 28);
      assertEquals(em.getEvaluationsMapFromCall("gennaio")("neri"), 30);
      // promossi di febbraio con voto
      assertEquals(em.getEvaluationsMapFromCall("febbraio").size,2);
      assertEquals(em.getEvaluationsMapFromCall("febbraio")("bianchi"),20);
      assertEquals(em.getEvaluationsMapFromCall("febbraio")("verdi"), 30);

      // tutti i risultati di rossi (attenzione ai toString!!)
      assertEquals(em.getResultsMapFromStudent("rossi").size ,3);
      assertEquals(em.getResultsMapFromStudent("rossi")("gennaio"),"FAILED");
      assertEquals(em.getResultsMapFromStudent("rossi")("febbraio"),"FAILED");
      assertEquals(em.getResultsMapFromStudent("rossi")("marzo"),"SUCCEEDED(25)");
      // tutti i risultati di bianchi
      assertEquals(em.getResultsMapFromStudent("bianchi").size,3);
      assertEquals(em.getResultsMapFromStudent("bianchi")("gennaio"),"RETIRED");
      assertEquals(em.getResultsMapFromStudent("bianchi")("febbraio"),"SUCCEEDED(20)");
      assertEquals(em.getResultsMapFromStudent("bianchi")("marzo"),"SUCCEEDED(25)");
      // tutti i risultati di neri
      assertEquals(em.getResultsMapFromStudent("neri").size,1);
      assertEquals(em.getResultsMapFromStudent("neri")("gennaio"),"SUCCEEDED(30L)");

    }

    // verifica del metodo ExamManager.getBestResultFromStudent
    @Test
    def optionalTestExamsManagement(): Unit = {
      this.prepareExams();
      // miglior voto acquisito da ogni studente, o vuoto..
      println(em.getBestResultFromStudent("bianchi"))
      assertEquals(em.getBestResultFromStudent("rossi"), Option(25));
      assertEquals(em.getBestResultFromStudent("bianchi"), Option(25));
      assertEquals(em.getBestResultFromStudent("neri"), Option(30));
      assertEquals(em.getBestResultFromStudent("viola"), Option.empty);
    }

  @Test
  def optionalTestCantCreateACallTwice(): Unit = {
    assertThrows(classOf[IllegalArgumentException], new Executable {
      override def execute(): Unit = {
        prepareExams();
        em.createNewCall("marzo");
      }
    })
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice(): Unit = {
    assertThrows(classOf[IllegalArgumentException], new Executable {
      override def execute(): Unit = {
        prepareExams();
        em.addStudentResult("gennaio", "verdi", erf.failed);
      }
    })
  }

}
