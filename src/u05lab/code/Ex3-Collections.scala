package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  import PerformanceUtils._

  /* Linear sequences: List, ListBuffer */
  val list:List[Int] = (1 to 1000000).toList
  measure(log("READING", "LIST")) {list.last}       // 19ms
  measure(log("ADDING ELEMENT", "LIST")) {list :+ 1} // 24ms
  measure(log("DROPPING LAST ELEMENT", "LIST")) {list.drop(list.size)} // 28ms

  println("")

  val listBuffer: ListBuffer[Int] =  scala.collection.mutable.ListBuffer[Int]()
  for(i <- list) { listBuffer += i }
  measure(log("READING", "LIST BUFFER")) {listBuffer.last} // 0ms
  measure(log("ADDING ELEMENT", "LIST BUFFER")) {listBuffer += 1} // 0ms
  measure(log("DROPPING LAST ELEMENT", "LIST BUFFER")) {listBuffer.drop(listBuffer.size)} // 34ms

  println("")

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  val vector: Vector[Int] = (1 to 1000000).toVector
  measure(log("READING", "VECTOR")) {vector.last} // 0ms
  measure(log("ADDING ELEMENT", "VECTOR")) {vector :+ 1} // 0ms
  measure(log("DROPPING LAST ELEMENT", "VECTOR")) {vector.drop(vector.length)} // 0ms

  println("")

  val array: Array[Int] =  (1 to 1000000).toArray
  measure(log("READING", "ARRAY")) {array.last} // 21ms
  measure(log("ADDING ELEMENT", "ARRAY")) {array :+ 1} // 5ms
  measure(log("DROPPING LAST ELEMENT", "ARRAY")) {array.drop(array.length)} // 0ms

  println("")

  val arrayBuffer: ArrayBuffer[Int] =  ArrayBuffer[Int]()
  for(i <- list) { arrayBuffer += i }
  measure(log("READING", "ARRAY BUFFER")) {arrayBuffer.last} // 0ms
  measure(log("ADDING ELEMENT", "ARRAY BUFFER")) {arrayBuffer :+ 1} // 1331ms
  measure(log("DROPPING LAST ELEMENT", "ARRAY BUFFER")) {arrayBuffer.drop(arrayBuffer.length)} // 3ms

  println("")

  /* Sets */
  val set: Set[Int] = (1 to 1000000).toSet
  measure(log("READING", "SET")) {set.last} // 2ms
  measure(log("ADDING ELEMENT", "SET")) {set + 1} // 0ms
  measure(log("DROPPING LAST ELEMENT", "SET")) {set.drop(set.size)} // 1ms

  println("")

  /* Maps */
  var map: Map[Int, Int] = Map()
  for(i <- list) { map += (i->i) }
  measure(log("READING", "MAP")) {map.last} // 2ms
  measure(log("ADDING ELEMENT", "MAP")) {map += (1->1)} // 0ms
  measure(log("DROPPING LAST ELEMENT", "MAP")) {map.drop(map.size)} // 76ms


   private def log(operation: String, string: String): String = "[ PERFORMANCE OF " +operation+ " " +string+ " ]  "
}