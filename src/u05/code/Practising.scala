package u05.code

object Practising extends App{

/*
    val l: List[Int] = List(10, 20, 30, 40, 50)

    println(l.getClass)
    println(l.isInstanceOf[scala.collection.immutable.List[_]])
    println(l)
    println(l.reverse)
    println(l.zipWithIndex)
    println(l.reverse.zipWithIndex)
    println(l.splitAt(3))
    println(l(2))
    println(l.reverse ::: 5 :: l)
    println(5 +: l)
    println(5 :: l)
    println(l.sortWith((e1, e2)=> e1%23==0))
    println(List.range(0,10))
    println(List.iterate(0,5)(_+6))

    import scala.collection.mutable.ListBuffer
    val fruits = ListBuffer[String]()
    fruits += "Apple"
    fruits += "Banana"
    fruits += ("Strawberry", "Kiwi")

    println(fruits.isInstanceOf[scala.collection.mutable.ListBuffer[_]])


    val s: Stream[Int] = Stream(10, 20, 30, 40, 50)
    println(s.getClass)
    println(s.isInstanceOf[scala.collection.immutable.Stream[_]])
    println(s)
    println(s(2))
    println(s)
    println(30 #:: s)
    println(s #::: s)
    println(s.toList)
    println((s #::: s).toList)


    def lazyNum[A](a: => A) = { println("u"+a); a}

    val s2: Stream[Int] = lazyNum(10) #:: lazyNum(20) #:: lazyNum(30) #:: lazyNum(40) #:: Stream.empty

    println(s2)
    println(s2(2))
    println(s2)
    val s3 = s2 #:::s2 // 10,20,30,?,10,20,30, ?
    println(s3)
    println(s3(2))
    println(s3(7))


    val v: Vector[Int] = Vector(10,20,30,40,50)
    println(v.getClass)
    println(v(2))
    println(v.reverse)

    val s: Set[Int] = Set(10,20,30,40,50)
    println(s.getClass)
    println(s)
    println(s+60+70)
    println(s & Set(100))
    println(s.contains(10))

    val m: Map[Int, String] = Map(10 -> "a", 20->"b", 30->"c")
    println(m.getClass)
    println(m)
    println(m.toVector)
    println(m.toList)
    println(m.keys)
    println(m.keySet)
    println(m.map { case (k,v) => (v, k+".")})
    println(m.map {case (k,v) => v+v})

    val some: Option[Int] = Some(10)
    val none: Option[Int] = None

    println(some.isDefined)
    println(none.isDefined)

    println(some.fold(-1)(_+1))
    println(none.fold(-1)(_+1))

    println(some.map(_+1))
    println(none.map(_+1))

    println(some.flatMap(x => if (x>0) Some(1) else None))
    println(none.flatMap(x => if (x>0) Some(1) else None))

    val some2: Option[Int] = Some(20)
    println(some flatMap (x => some2 flatMap( (y => Some(x*y)))))



  class C2 { val a = 1 }

  abstract class D2{ val b = 2 }

  object O2 { val c = 3 }

  trait T2 { val d = 4 }

  def f2(): Unit = { println("hello")}

  def g2(a: {val e:Int}): Int = a.e

  println(new C2().a, new D2{}.b, O2.c, new T2{}.d, f2(), g2(new {val e = 10}))



  class C(var v: Int){
      class D {
        def g(n: Int): Int = n + C.this.v
      }
  }

  val c: C = new C(5)
  println(new c.D().g(4))

  object O {
    var v: Int  = 10

    class D {
      def g(n: Int): Int = n + O.v
    }
  }

  println(new O.D().g(4))

 */

  trait Comparator[A] {
    def compare(a: A, b: A): Int
  }

  def min[A](list: List[A], comp: Comparator[A]): Option[A] = {
    def min2(list: List[A], minSoFar: A): A = list match {
      case h::t if (comp.compare(h, minSoFar)>0) => min2(t, minSoFar)
      case h::t => min2(t, h)
      case _ => minSoFar
    }

    Some(list) filter(! _.isEmpty) map (l => min2(l.tail, l.head))
  }
}













